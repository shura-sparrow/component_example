
import * as React from 'react';
import { noop } from '../../common/utils';
import MultiSelectTag, { MultiSelectTagProps } from './MultiSelectTag';
import * as styles from './MultiSelectTag.css';
import Focusable from '../Focusable/Focusable';
import { KeyEvent as KeyCode } from '../../common/KeyboardEvent';
import { ElementSize } from '../../common/Models';

export interface MultiSelectTagsProps {
    /* Значения */
    items?: any[];

    /** Размер */
    size?: ElementSize;

    /** Коллбэк на изменение  */
    onChange?: (value: any[], e?: React.SyntheticEvent<any>) => void;

    /** Коллбэк на переход к тегам */
    onLeave?: () => void;

    /** Коллбэк на клик по тегу */
    showSelected?: (value: any) => void;
}

export interface Item {
    tag?: React.ReactElement<MultiSelectTagProps>;
    index?: number;
}

export interface State {
    /**
     * Индекс текущего тега.
     * Особое значение -1 означает, что ни один тег не выбран.
    */
    currentIndex?: number;
}

export default class MultiSelectTags extends React.PureComponent<MultiSelectTagsProps, State> {
    state: State = {
        currentIndex: null
    };

    static defaultProps = {
        showSelected: noop,
        onLeave: noop,
        onChange: noop
    };

    private getCurrent(value: string): Item {
        let tags = React.Children.toArray(this.props.children) as React.ReactElement<any>[];
        let tag: Item = {};
        for (let i = 0; i < tags.length; i++) {
            if (tags[i].props.value === value) {
                return {
                    index: i,
                    tag: tags[i]
                };
            }
        }
        return tag;
    }

    private removeTag = (value: string, event?: React.KeyboardEvent<HTMLDivElement>) => {
        const { onChange } = this.props;
        const selected = this.props.items || [];
        let current = this.props.items.indexOf(value);
        let currentIndex;

        if (!event || event && event.keyCode === KeyCode.DOM_VK_BACK_SPACE) {
            currentIndex = current - 1 >= 0 ? current - 1 : current + 1 < this.props.items.length ? current : -1;
        } else if (event && event.keyCode === KeyCode.DOM_VK_DELETE) {
            currentIndex = current + 1 < this.props.items.length ? current : current - 1 >= 0 ? current - 1 : -1;
        }

        this.setState({
            currentIndex
        });

        onChange(selected.filter(this.filterValue(value)), event);
    }

    private filterValue = (value: string) => item => item !== value;

    private onHandleClick = (value: string) =>
        (event: React.MouseEvent<any>) => {
            if (this.getCurrent(value).tag.props.disabled) {
                return false;
            }
            this.showSelected(value);
    }

    private onHandleKeyDown = (value: string) =>
        (event: React.KeyboardEvent<any>) => this.handleKeyDown(value, event)

    private renderTag = (itemValue: string, index: number) => {
        const tag = this.getCurrent(itemValue).tag;

        if (!tag) {
            return null;
        }

        const { value, children, disabled } = tag.props;

        return (
            <MultiSelectTag
                size={this.props.size}
                focused={this.state.currentIndex === index}
                onRemove={this.removeTag}
                onClick={this.onHandleClick(value)}
                onKeyDown={this.onHandleKeyDown(value)}
                onDown={this.onDown}
                key={value}
                value={value}
                disabled={disabled}
            >
                {children}
            </MultiSelectTag>
        );
    }

    private showSelected = (value: string) => {

        this.setState({ currentIndex: -1 });

        this.props.showSelected(value);
        this.props.onLeave();
    }

    private onDown = (event: React.KeyboardEvent<HTMLElement>) => {
        event.preventDefault();
        this.setState({ currentIndex: -1 });
        this.props.onLeave();
        return false;
    }

    private handleKeyDown = (itemValue: string, event?: React.KeyboardEvent<HTMLDivElement>) => {
        switch (event.keyCode) {
            case KeyCode.DOM_VK_DELETE:
            case KeyCode.DOM_VK_BACK_SPACE:
            case KeyCode.DOM_VK_RETURN:
            case KeyCode.DOM_VK_SPACE:
                event.preventDefault();
                this.removeTag(itemValue, event);
                break;

            default:
                break;
        }
    }

    render() {
        const { items } = this.props;
        return (
            items && items.length ?
                <Focusable>
                    <div className={styles.tags}>
                        {items.map(this.renderTag)}
                    </div>
                </Focusable> :
                null
        );
    }
}