import * as React from 'react';
import { noop } from '../../common/utils';
import Focusable, { Props as FocusableProps } from '../Focusable/Focusable';
import { Tag as CoreTag, TagSize, TagRemoveButton } from 'ufs-core-ui-next';
import * as tagStyles from './MultiSelectTag.css';
import { ElementSize } from '../../common/Models';

export class ThemedRemoveButton extends TagRemoveButton {
    protected theme = tagStyles;
}

export class ThemedTag extends CoreTag {
    protected theme = tagStyles;
    protected RemoveButton = ThemedRemoveButton;
}

export interface MultiSelectTagProps extends React.Props<MultiSelectTag>, FocusableProps {
    /* Коллбэк, выполняемый при удалении тега */
    onRemove?: (value: any, event: React.KeyboardEvent<HTMLDivElement>) => void;

    /** Коллбэк, выполняемый при клике на тег */
    onClick?: (event: React.MouseEvent<HTMLElement>) => void;

    /* Значение */
    value?: any;

    /* Флаг фокуса компонента */
    focused?: boolean;

    /* Размер */
    size?: ElementSize;
}

export default class MultiSelectTag extends React.PureComponent<MultiSelectTagProps, {}> {

    static defaultProps = {
        onRemove: noop,
        focused: false,
        size: ElementSize.MD,
        value: null
    };

    private removeButton: HTMLButtonElement;
    private saveRemoveButton = (button: HTMLButtonElement) => this.removeButton = button;

    private onRemove = (event: React.MouseEvent<HTMLButtonElement> | React.KeyboardEvent<HTMLDivElement>) =>
        this.props.onRemove(this.props.value, event as React.KeyboardEvent<HTMLDivElement>)

    private handleMouseDown = (event: React.MouseEvent<HTMLDivElement>) => {
        if (this.removeButton && !this.removeButton.contains(event.target as Node)) {
            event.preventDefault();
        }
    }

    private tagRefs = { remove: this.saveRemoveButton };

    render() {
        const { focused, size, children, disabled, onDown, onKeyDown, onClick, tabIndex } = this.props;

        const tagSize = (size === ElementSize.SM) ? TagSize.SM : TagSize.MD;

        return (
            <Focusable
                tabIndex={tabIndex}
                disabled={disabled}
                autoFocus={focused}
                onDown={onDown}
                onKeyDown={onKeyDown}
            >
                <span>
                    <ThemedTag
                        size={tagSize}
                        refs={this.tagRefs}
                        disabled={disabled}
                        onClick={onClick}
                        onRemove={this.onRemove}
                        onMouseDown={this.handleMouseDown}
                    >
                        {children}
                    </ThemedTag>
                </span>
            </Focusable>
        );
    }
}